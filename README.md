# README #

Spike to store and present time series data from fitness tracking devises.
One example would be to steps count from fitbit or similar devises. Currently not integrated with fitbit but the idea is to test storing and fetch time series data from rdbms table


### Application setup (local)

* Install rvm
* Install ruby 2.1 + using rvm
* Create a gemset for this application
* Create .ruby-version and .ruby-gemset files

Switch the directory and cd to app directory to get the correct version of ruby/gemset for rvm

bundle install
rake db:migrate

Currenly using sqlite3. Setting up mysql in not required


### Setup sample data

rake db:seed

Execute the following with a few times to bump the steps count for users. Defaults to user id = 1
Update the json request (data/post_steps.json) to set specific value to bump on every call

curl -H "Content-Type: application/json" -X POST --data "@data/post_steps.json" http://localhost:3000/apis/steps.json
