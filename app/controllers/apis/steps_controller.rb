class Apis::StepsController < ApplicationController
  # TODO no validation applied now. need to refactor
  def create
    @user = User.where(id: params[:user_id]).first
    count = params[:count].to_i
    if DataStat.add_step(@user, count)
      render json: { new_value: DataStat.steps(@user) }, status: 201
    else
      render json: { error: 'failed to update steps' }, status: 422
    end
  end
end
