class DashController < ApplicationController

  before_filter :fetch_users
  before_filter :load_stats

  def index
  end

  private

  def load_stats
    y, m, d = DateTime.now.strftime("%y.%m.%d").split(".")
    @stats = {}
    @users.each do |user|
      @stats[user.id] = {
        total_steps: DataStat.steps(user),
        year_steps: DataStat.steps(user, y),
        month_steps: DataStat.steps(user, y, m),
        day_steps: DataStat.steps(user, y, m, d)
      }
    end
  end

  # TODO - paginate 
  def fetch_users
    @users = User.all
  end

end
