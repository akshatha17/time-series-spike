class DataStat < ActiveRecord::Base

  # Key name
  #   USR.[USERID].[YY].[MM].[DD].steps
  #   USR.[USERID].[YY].[MM].steps
  #   USR.[USERID].[YY].steps
  #   USR.[USERID].steps
  def self.add_step(user, count, y=nil, m=nil, d=nil)
    y1, m1, d1 = DateTime.now.strftime("%y.%m.%d").split(".")
    y ||= y1; m ||= m1; d ||= d1

    key_prefix = "USR.#{user.id}"
    key_sufix = "steps"

    # TODO 
    # optimize the database calls. can do in batches
    # should go in one transaction or batch
    # refactor code
    keys = [[y], [y, m], [y,m,d] ].collect do |k_a|
      k_a.join(".")
    end

    add_to("#{key_prefix}.#{key_sufix}", count)
    keys.each do |k|
      key_name = "#{key_prefix}.#{k}.#{key_sufix}"
      add_to(key_name, count)
    end
    true  # this should depend on the return value of .update calls or .add_to
  end

  # Read steps
  # TODO validation to y, m, d . Refactor
  def self.steps(user, y=nil, m=nil, d=nil)
    key_prefix = "USR.#{user.id}"
    key_sufix = "steps"
    key = [y,m,d].compact.join(".")
    key_name = key.present? ? "USR.#{user.id}.#{key}.steps" : "USR.#{user.id}.steps"
    where(name: key_name).first.try(:value) || 0
  end

  private
  def self.add_to(n, count)
    puts "here"
    ds = where(name: n).first_or_create
    ds.update value: ds.value + count
  end
end
