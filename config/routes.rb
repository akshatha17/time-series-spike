Rails.application.routes.draw do

  namespace :apis do
    resources :steps, only: :create
  end

  root "dash#index"

end
