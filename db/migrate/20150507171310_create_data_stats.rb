class CreateDataStats < ActiveRecord::Migration
  def change
    create_table :data_stats do |t|
      t.string :name
      t.integer :value, default: 0

      t.timestamps null: false
    end

    # won't be effective if the name goes beyond 20 char length
    add_index :data_stats, :name
  end
end
